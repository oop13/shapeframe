/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Sarocha
 */
public class RectangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);

        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        frame.add(txtWidth);

        JLabel lblLength = new JLabel("length:", JLabel.TRAILING);
        lblLength.setSize(50, 20);
        lblLength.setLocation(5, 30);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        frame.add(lblLength);

        final JTextField txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 30);
        frame.add(txtLength);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle width= ??? length= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 70);
        lblResult.setBackground(Color.PINK);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    double width = Double.parseDouble(strWidth);
                    String strLength = txtLength.getText();
                    double length = Double.parseDouble(strLength);
                    Rectangle rectangle = new Rectangle(width, length);
                    lblResult.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth())
                            + "Rectangle length = " + String.format("%.2f", rectangle.getLength())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showConfirmDialog(frame, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                }
            }

        });

        frame.setVisible(true);
    }
}
