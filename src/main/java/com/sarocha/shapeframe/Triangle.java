/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeframe;

/**
 *
 * @author Sarocha
 */
public class Triangle extends Shape {

    private double tri = Math.sqrt(3) / 4;
    private double side;

    public Triangle(double side) {
        super("Triangle");
        this.side = side;
    }

    public double getTri() {
        return tri;
    }

    public void setTri(double tri) {
        this.tri = tri;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double calArea() {
        return tri * Math.pow(side, 2);
    }

    @Override
    public double calPerimeter() {
        return side + side + side;
    }

}
