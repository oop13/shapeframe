/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Sarocha
 */
public class TriangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(40, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        frame.add(lblSide);
        
        final JTextField txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(50, 5);
        frame.add(txtSide);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(110, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle side= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
         btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);
                    Triangle triangle = new Triangle(side);
                    lblResult.setText("Triangle side = " + String.format("%.2f", triangle.getSide())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showConfirmDialog(frame, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }

        });

        frame.setVisible(true);
    }
}
